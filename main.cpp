#include <iostream>
#include <fstream>
#include "symulator.h"
#include <cstdlib> // do GNUPlota
#include <string>

using namespace std;

const double pi=3.14159265358979323846;

int main()
{

    /*Czyszczenie pliku z wynikami*/
    ofstream plik;
    plik.open("plik.txt"); //UWAGA: TRZEBA ZAMIENIC NA SCIEZKE BEZWGLEDNA
    plik.close();

    int blad=0;
    double x, y, z, v, alpha1, beta1, alpha2, beta2, w, k,  m,  l;
    cout<<"Witamy w uproszczonym symulatorze rzutu ukosnego.\nSymulator zawiera opor powietrza i staly wiatr\n";
    cout<<"Jednostki podawanych wielkosci:\nmetr, stopnie, metr na sekunde, kilogram, sekunda\n";
    cout <<"Wektory predkosci przyjmowane sa we wspolrzednych sferycznych\n";
    cout <<"Wyniki wyswietlane w formacie: t X Vx Y Vy Z Vz\n\n";
    do{
        if(blad){
            std::cin.clear();
            std::cin.sync();
            cout<<"Bledne dane\n";
            blad=0;
        }
        cout<<"Podaj polozenie poczatkowe\n-x\n-y\n-z(os pionowa):\n";
        cin>>x;
        cin>>y;
        cin>>z;
    }while((x!=0 && int(x)==0 && !(x<1 || x>-1) && (blad=1)) ||
           (y!=0 && int(y)==0 && !(y<1 || y>-1) && (blad=1)) ||
           (z!=0 && int(z)==0 && !(z<1 || z>-1) && (blad=1)));

    do{
        if(blad){
            std::cin.clear();
            std::cin.sync();
            cout<<"Bledne dane\n";
            blad=0;
        }
        cout<<"Podaj predkosc poczatkowa ciala\n-kat wzgledem osi x na plaszczyznie xy (0-360 stopni)\n-kat wzgledem osi z (0-180 stopni)\n-wartosc predkosci (modul wektora):\n";
        cin>>alpha1;
        cin>>beta1;
        cin>>v;
    }while((alpha1!=0 && int(alpha1)==0 && !(alpha1<1) && (blad=1)) ||
           (beta1!=0 && int(beta1)==0 && !(beta1<1) && (blad=1)) ||
           (v!=0 && int(v)==0 && !(v<1 || v>-1) && (blad=1)) ||
           ((alpha1<0 || alpha1> 360) && (blad=1)) ||
           ((beta1<0 || beta1> 180) && (blad=1)));

    do{
        if(blad){
            std::cin.clear();
            std::cin.sync();
            cout<<"bledne dane\n";
            blad=0;
        }
        cout<<"Podaj predkosc wiatru\n-kat wzgledem osi x na plaszczyznie xy (0-360 stopni)\n-kat wzgledem osi z (0-180 stopni)\n-wartosc predkosci (modul wektora):\n";
        cin>>alpha2;
        cin>>beta2;
        cin>>w;
    }while((alpha2!=0 && int(alpha2)==0 && !(alpha2<1) && (blad=1)) ||
           (beta2!=0 && int(beta2)==0 && !(beta2<1) && (blad=1)) ||
           (w!=0 && int(w)==0 && !(w<1 || w>-1) && (blad=1)) ||
           ((alpha2<0 || alpha2> 360) && (blad=1)) ||
           ((beta2<0 || beta2> 180) && (blad=1)));

    do{
        if(blad){
            std::cin.clear();
            std::cin.sync();
            cout<<"bledne dane\n";
            blad=0;
        }
        cout<<"Podaj pozostale parametry\n-wspolczynnik oporu wiatru\n-mase ciala\n-zakres czasu dla jakiego program ma liczyc (liczba sekund):\n";
        cin>>k;
        cin>>m;
        cin>>l;
    }while((k!=0 && int(k)==0 && !(k<1) && (blad=1)) ||
           (m!=0 && int(m)==0 && !(m<1) && (blad=1)) ||
           (l!=0 && int(l)==0 && !(m<1) && (blad=1)) ||
           ((k<0) && (blad=1)) ||
           ((m<0) && (blad=1)) ||
           ((l<0) && (blad=1)));


    alpha1=(alpha1/360)*2*pi;
    beta1=(beta1/360)*2*pi;
    alpha2=(alpha2/360)*2*pi;
    beta2=(beta2/360)*2*pi;

    symulator sim1(x, y, z, v, alpha1, beta1, alpha2, beta2, w, k,  m,  l);
    sim1.rozwiaz();

    /* Skrypt GNUPlota rysujacy trajektorie */
    string command = "gnuplot --persist -e \"load 'skrypt.gp' \""; //UWAGA: TRZEBA ZAMIENIC NA SCIEZKE BEZWGLEDNA
    system(command.c_str()); //wywolanie komendy

    return 0;
}


